import React from "react"

import { Card, Typography } from "antd"

import { CenterView } from "../../../layouts"

const Home = () => {
  return (
    <CenterView>
      <Card>
        <Typography.Title level={1}>Welcome to Sonar !</Typography.Title>
        This is WIP
      </Card>
    </CenterView>
  )
}

export default Home
