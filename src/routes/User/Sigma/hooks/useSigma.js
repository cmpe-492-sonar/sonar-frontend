import React, { useEffect, useState, useRef } from "react"
import Sigma from "sigma"
import useSigmaNodeEdgeUpdates from "./private/updateSigmaNodesEdges"

const useSigma = ({ nodes, edges }) => {
  const sigma = useRef(new Sigma())
  window.ss = sigma
  const [renderer, setRenderer] = useState(null)
  const containerRef = useRef((container) => {
    console.log("cont ref", container)
    if (container) {
      const re = sigma.current.addRenderer({
        type: "canvas",
        container,
      })
      console.log("re", re)
      setRenderer(re)
      sigma.current.refresh()
    } else {
      sigma.current.killRenderer(renderer)
    }
  })
  useSigmaNodeEdgeUpdates(sigma.current, nodes, edges)

  useEffect(() => {
    return () => {
      console.log("noo i die")
      sigma.current.kill()
    }
  }, [])

  return [containerRef.current, sigma.current]
}

export default useSigma
