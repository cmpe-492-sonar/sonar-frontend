import React, { useEffect, useRef } from "react"
import { updateProperties } from "../../helpers"

const useSigmaNodeEdgeUpdates = (sigma, nodes, edges) => {
  const graphMaps = useRef({
    nodeMap: new Map(),
    edgeMap: new Map(),
  })
  useEffect(() => {
    const nodeMap = new Map()
    const edgeMap = new Map()
    if (sigma) {
      nodes.forEach(({ id, ...nodeFieldsRaw }) => {
        const nodeFieldsMap = new Map(Object.entries(nodeFieldsRaw))
        if (graphMaps.current.nodeMap.has(id)) {
          // update node
          const node = sigma.graph.nodes(id)
          const oldNodeFields = graphMaps.current.nodeMap.get(id)
          console.log("update node")
          updateProperties(node, oldNodeFields, nodeFieldsMap)
        } else {
          // add node
          console.log("added node")
          sigma.graph.addNode({
            id,
            x: Math.random(),
            y: Math.random(),
            ...nodeFieldsRaw,
          })
        }
        nodeMap.set(id, nodeFieldsMap)
      })
      edges.forEach(({ id, source, target, ...edgeFieldsRaw }) => {
        const edgeFieldsMap = new Map(Object.entries(edgeFieldsRaw))
        if (graphMaps.current.edgeMap.has(id)) {
          // update edge
          console.log("update edge")
          const edge = sigma.graph.edges(id)
          const oldEdgeFields = graphMaps.current.edgeMap.get(id)
          updateProperties(edge, oldEdgeFields, edgeFieldsMap)
        } else {
          // add edge
          console.log("add edge")
          sigma.graph.addEdge({
            id,
            source,
            target,
            ...edgeFieldsRaw,
          })
        }
        edgeMap.set(id, edgeFieldsMap)
      })
      graphMaps.current.edgeMap.forEach((edge, id) => {
        if (!edgeMap.has(id)) {
          // delete edge
          console.log("delete edge")
          sigma.graph.dropEdge(id)
        }
      })
      graphMaps.current.nodeMap.forEach((node, id) => {
        if (!nodeMap.has(id)) {
          // delete node
          console.log("delete node")
          sigma.graph.dropNode(id)
        }
      })
    }
    // eslint-disable-next-line no-param-reassign
    graphMaps.current = { nodeMap, edgeMap }
    if (sigma) {
      sigma.refresh()
      console.log("refreshed")
    }
  }, [sigma, nodes, edges])
}

export default useSigmaNodeEdgeUpdates
