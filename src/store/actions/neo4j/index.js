import { SET_NEO4J_AUTH } from "../../types"

export const setNeo4jAuth = (data) => ({
  type: SET_NEO4J_AUTH,
  data,
})
