import { combineReducers } from "redux"

import neo4j from "./neo4j"
import neovis from "./neovis"

export default combineReducers({
  neo4j,
  neovis,
})
